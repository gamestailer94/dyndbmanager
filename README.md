OXID DynDBManager
=================
Disclamer
---------
This Class is Static callable

Install
-------
1. Place TP Folder in "modules" Folder
2. Activate Module

How to use
==========

CheckField Function
-------------------
Checks if Field exist in given Table with given Metadata, if not creates it with given data.

Should only be used on already existing Tables.

$tableName string Name of Table to Check in

$fieldName string Name of Field to Check for

$fieldType string Type of Field to Check for

$fieldLength int Length of the Field to Check

$nullable bool Is the Field Nullable

$default string|null Default Value if set

Example
-------
	dynDBManger::checkField('oxuser','oxid','char',32,false,null)  

CheckTable Function
-------------------
Checks if table exist and if fields are correct set.

$tableName string Name of Table

$fields array Array of Field Data

Format
-------
"FieldName" => array(Type,Length,nullable,default)

Example
-------
	dynDBManger::checkTable('test_table'array('oxid'=>'array('char',32,false,null))

UpdateViews Function
--------------------
Updates Views.

Should always be called AFTER all check are done.

Only Updates if changes ware made to DB or Forced.

$force bool Force Update