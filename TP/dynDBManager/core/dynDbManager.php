<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Tim Platzke <tim@platzke.de>
 *
 *  All rights reserved
 *
 *
 * @TODO: add key check and create function
 * @TODO: add default primary key to oxid
 ***************************************************************/

class dynDbManager {

	protected static $changed = false;

	/**
	 * @var array Field Types to SQL Keywords
	 */

	static private $fieldTypes = array(
		"varchar" => "VARCHAR",
		"int" => "INT",
		"bool" => "TINYINT",
		"tinyint" => "TINYINT",
		"timestamp" => "TIMESTAMP",
		"double" => "DOUBLE",
		"date" => "DATE",
		"float" => "FLOAT",
		"datetime" => "DATETIME",
		"char" => "CHAR",
		"bigint" => "BIGINT",
		'text'=> "TEXT"
	);

	/**
	 * Checks if Field exist in given Table, if not creates it with given data
	 *
	 * @param $tableName string Name of Table to Check in
	 * @param $fieldName string Name of Field to Check for
	 * @param $fieldType string Type of Field to Check for
	 * @param $fieldLength int Length of the Field to Check
	 * @param $nullable bool Is the Field Nullable
	 * @param $default string|null Default Value if set
	 */

	static public function checkField($tableName,$fieldName,$fieldType = "varchar",$fieldLength = 255, $nullable = false, $default = null){
		/** @var oxDbMetaDataHandler $Db */
		$Db = oxNew('oxDbMetaDataHandler');
		/**
		 * check if field exist
		 */
		if($Db->fieldExists($fieldName,$tableName)){
			$meta = self::_getFieldMeta($tableName,$fieldName);

			$null = $nullable?"YES":"NO";
			preg_match("/([a-z]{1,})\((\d{1,})\)/i",$meta["Type"],$type);

			$sqlType = strtoupper(self::$fieldTypes[strtolower($fieldType)]);

			/**
			 * set length if special field type is set
			 */
			if(strtolower($fieldType) == "bool") {
				$fieldLength = 1;
			}else if($sqlType == "TEXT" || $sqlType == "FLOAT" || $sqlType == "DATETIME" || $sqlType == "DOUBLE" || $sqlType == "DATE" || $sqlType == "TIMESTAMP"){
				$fieldLength = $type[2] = null;
				$type[1] = $sqlType;
			}

			if($default == '')
				$default = null;

			/**
			 * check if update is needed
			 */
			if($meta['Null'] != $null || strtolower($type[1]) != strtolower(self::$fieldTypes[strtolower($fieldType)]) || $fieldLength != $type[2] || $meta["Default"] != $default )
				self::_updateField($tableName,$fieldName,$fieldType,$fieldLength,$nullable,$default);
		}else{
			self::_createField($tableName,$fieldName,$fieldType,$fieldLength,$nullable,$default);
		}
	}

	/**
	 * checks if table exist and if fields are correct set
	 *
	 * @param string $tableName
	 * @param array $fields List of Fields Format: "FieldName" => array(Type,Length,nullable,default)
	 *
	 * @return bool
	 */
	static public function checkTable($tableName, $fields){
		/** @var oxDbMetaDataHandler $Db */
		$Db = oxNew('oxDbMetaDataHandler');
		if(!$Db->tableExists($tableName)){
			self::_createTable($tableName,$fields);
		}else{
			foreach($fields as $fieldName => $meta) {
				if(!isset($meta[1]))
					$meta[1] = 255;
				if(!isset($meta[2]))
					$meta[2] = false;
				if(!isset($mate[3]))
					$meta[3] = null;
				self::checkField($tableName,$fieldName,$meta[0],$meta[1],$meta[2],$meta[3]);
			}
		}
		return false;
	}

	/**
	 * generates and runs update or create statement
	 *
	 * @param $tableName
	 * @param $fieldName
	 * @param $fieldType
	 * @param $fieldLength
	 * @param $nullable
	 * @param $default
	 * @param $update
	 */
	static private function _createField($tableName,$fieldName,$fieldType,$fieldLength,$nullable,$default,$update=false){
		if($update)
			$sql = "ALTER TABLE `$tableName` MODIFY `$fieldName` ";
		else
			$sql = "ALTER TABLE `$tableName` ADD `$fieldName` ";

		$type = self::$fieldTypes[strtolower($fieldType)];

		if(strtolower($fieldType) == "bool")
			$fieldLength = 1;
		else if($type == "FLOAT" || $type == "DATETIME" || $type == "DOUBLE" || $type == "DATE" || $type == "TIMESTAMP")
			$fieldLength = null;

		if(!is_null($fieldLength))
			$sql .= "$type($fieldLength) ";
		else
			$sql .= "$type ";

		if(!$nullable)
			$sql .= "NOT NULL ";

		if($default != null)
			$sql .= "DEFAULT '$default'";

		oxDb::getDb()->query($sql);

		self::$changed = true;
	}

	/**
	 * calls create table with update param
	 *
	 * @param $tableName
	 * @param $fieldName
	 * @param $fieldType
	 * @param $fieldLength
	 * @param $nullable
	 * @param $default
	 */
	static private function _updateField($tableName,$fieldName,$fieldType,$fieldLength,$nullable,$default){
		self::_createField($tableName,$fieldName,$fieldType,$fieldLength,$nullable,$default,true);
	}

	/**
	 * returns field meta data
	 *
	 * @param $tableName
	 * @param $fieldName
	 * @return array
	 */
	static private function _getFieldMeta($tableName,$fieldName){
		$sql = "SHOW FIELDS FROM `$tableName` WHERE Field = '$fieldName'";

		$db = oxDb::getDb(oxdb::FETCH_MODE_ASSOC);

		return $db->getAll($sql)[0];
	}

	/**
	 * generates and runs create table statement
	 *
	 * @param $tableName
	 * @param $fields
	 */
	static private function _createTable($tableName,$fields){
		$sql = "CREATE TABLE $tableName (";
		$first = true;

		foreach($fields as $fieldName => $meta) {
			if($first)
				$first = false;
			else
				$sql .= ", ";

			$fieldType = $meta[0]?:'varchar';
			$fieldLength = $meta[1]?:255;
			$nullable =  (bool) $meta[2];
			$default = $meta[3]?:null;

			$sql .= "$fieldName ";
			$type = self::$fieldTypes[strtolower($fieldType)];

			if( strtolower( $fieldType ) == "bool" )
				$fieldLength = 1;
			else if( $type == "FLOAT" || $type == "DATETIME" || $type == "DOUBLE" || $type == "DATE" || $type == "TIMESTAMP" )
				$fieldLength = NULL;

			if( !is_null( $fieldLength ) )
				$sql .= "$type($fieldLength) ";
			else
				$sql .= "$type ";

			if( !$nullable )
				$sql .= "NOT NULL ";

			if( $default != NULL )
				$sql .= "DEFAULT '$default'";
		}
		$sql .= ") CHARACTER SET latin1 COLLATE latin1_general_ci;";

		oxDB::getDb()->query($sql);

		self::$changed = true;
	}

	/**
	 * updates Views if DB structure was changed
	 *
	 * @param bool $force Forces update
	 */
	public static function updateViews($force = false){
		/** @var oxDbMetaDataHandler $Db */
		$Db = oxNew('oxDbMetaDataHandler');
		if($force || self::$changed)
			$Db->updateViews();
	}

}