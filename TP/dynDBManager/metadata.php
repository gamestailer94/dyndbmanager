<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Tim Platzke <tim@platzke.de>
 *
 *  All rights reserved
 **************************************************************/
$sMetadataVersion = '1.2';

/**
 * Module information
 */
$aModule = array(
	'id'           => 'TP_DynDBManager',
	'title'        => 'TP | Dynamischer DB Manager',
	'description'  => array(
	),
//	'thumbnail'    => '',
	'version'      => '0.1.1 Alpha',
	'author'       => 'Tim Platzke',
	'url'          => 'http://www.platzke.de',
	'email'        => 'tim@platzkede',
	'extend'       => array(
	),
	'files' => array(
		'dynDbManager' => 'TP/dynDbManager/core/dynDbManager.php'
	),
	'events'       => array(
//		'onActivate'   => '',
//		'onDeactivate' => ''
	),
	'templates' => array(
	),
	'blocks' => array(
	),
	'settings' => array(
	)
);
